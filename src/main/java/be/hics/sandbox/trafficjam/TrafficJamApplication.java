package be.hics.sandbox.trafficjam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrafficJamApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrafficJamApplication.class, args);
        System.out.println(Dinglemouse.trafficJam("abcdefghijklmnoXqr", new String[]{"","BBBB","CCCCC","DDDDDD"}));
        System.out.println("abBcBCBdBCDCeCDCfDgDhDiDjklmnoX");
    }
}
