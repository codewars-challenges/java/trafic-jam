package be.hics.sandbox.trafficjam;

import java.util.Arrays;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Dinglemouse {

    public static String trafficJam(final String mainRoad, final String[] sideStreets) {
        // Util.display(mainRoad, sideStreets);
        // Your code here
        int myPos = mainRoad.indexOf("X");
        String[] main = mainRoad.substring(0, myPos + 1).split("");
        String[] side = Arrays.copyOf(sideStreets, Math.min(myPos, sideStreets.length));
        for (int step = 0; step < myPos; step++) {
            for (int i = 1; step + i <= myPos; i++) {
                if (side.length > i - 1 && side[i - 1].length() > 0) {
                    String[] zipped = rzip(main[step + i], side[i - 1]);
                    main[step + i] = zipped[0];
                    side[i - 1] = zipped[1];
                }
            }
        }
        return Arrays.stream(main).collect(Collectors.joining());
    }

    public static String[] rzip(final String destination, final String source) {
        String[] result = new String[2];
        IntFunction<String> zip = i -> (source.length() - 1 - i >= 0 ? source.substring(source.length() - 1 - i, source.length() - i) : "") + destination.substring(i, i + 1);
        result[0] = IntStream.range(0, destination.length()).mapToObj(zip).collect(Collectors.joining());
        result[1] = source.length() > destination.length() ? source.substring(0, source.length() - destination.length()) : "";
        return result;
    }

}
